package gitrepo.com.gitrepo;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ContributorDetailActivity extends AppCompatActivity {

    Context context;
    RecyclerView rv_repos;
    RepoListAdapter adapter;
    List<RepoModel> repoLIst = new ArrayList<>();
    ImageView iv_contri;
    TextView tv_contri;
    ContributorsModel contributorsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributor_detail);

        context         = this;

        rv_repos        = (RecyclerView) findViewById(R.id.rv_repos);
        iv_contri       = (ImageView) findViewById(R.id.iv_contri);
        tv_contri       = (TextView) findViewById(R.id.tv_contri);

        if (getIntent().getExtras()!=null){
            contributorsModel = getIntent().getParcelableExtra("contriData");
            getSupportActionBar().setTitle(contributorsModel.getContri_name());

            Picasso.with(context).load(contributorsModel.getImage_url()).into(iv_contri);
            tv_contri.setText(contributorsModel.getContri_name());

            new GetContriRepos().execute();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public class GetContriRepos extends AsyncTask<String, Void, String> {
        String result="";
        ProgressDialog progressDialog = new ProgressDialog(context);

        public GetContriRepos() {
            super();
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                String postURL = contributorsModel.getRepo_url();
                HttpGet get = new HttpGet(postURL);


                HttpResponse responsePOST = client.execute(get);
                //  HttpEntity resEntity = responsePOST.getEntity();

                result = EntityUtils.toString(responsePOST.getEntity());
                Log.i("result",result);

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result != null){
                try {
                   // JSONObject repositoryData = new JSONObject(result);
                    JSONArray repoArr = new JSONArray(result);
                    for (int i = 0; i<repoArr.length();i++){
                        JSONObject data = repoArr.getJSONObject(i);
                        // Log.e("datas",data.toString());
                        System.out.println(data.toString());
                        RepoModel repoModel = new RepoModel();
                        repoModel.setRepo_name(data.getString("name"));
                        repoModel.setRepo_full_name(data.getString("full_name"));
                        repoModel.setRepo_description(data.getString("description"));
                        repoModel.setViews(data.getString("watchers_count"));
                        repoModel.setForks(data.getString("forks_count"));
                        repoModel.setStars(data.getString("stargazers_count"));
                        repoModel.setProject_url(data.getString("html_url"));
                        repoModel.setContributor_url(data.getString("contributors_url"));
                        JSONObject owner = data.getJSONObject("owner");
                        repoModel.setImage_url(owner.getString("avatar_url"));
                        repoLIst.add(repoModel);
                    }

                    adapter = new RepoListAdapter(context,repoLIst);
                    rv_repos.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
