package gitrepo.com.gitrepo;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class RepoListAdapter extends RecyclerView.Adapter<RepoListAdapter.MyViewHolder> implements Filterable {

    private List<RepoModel> originalList;

    private List<RepoModel> filterRepoList;

    Context context;

    private RepoFilter repoFilter;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_name,tv_repo_full_name,tv_views,tv_fork,tv_stars;
        ImageView iv_avtar;
        CardView cardView;
        LinearLayout ll_card,ll_fill,ll_edit;
        FrameLayout frame;

        public MyViewHolder(View view) {
            super(view);
            tv_name             = (TextView) view.findViewById(R.id.tv_repo_name);
            tv_repo_full_name   = (TextView) view.findViewById(R.id.tv_repo_full_name);
            tv_views            = (TextView) view.findViewById(R.id.tv_views);
            tv_fork             = (TextView) view.findViewById(R.id.tv_fork);
            tv_stars            = (TextView) view.findViewById(R.id.tv_stars);
            iv_avtar            = (ImageView) view.findViewById(R.id.iv_avtar);

            cardView            = (CardView) view.findViewById(R.id.card_view);

        }
    }

    public RepoListAdapter(Context context,List<RepoModel> originalList) {
        this.originalList = originalList;
        this.filterRepoList = originalList;
        this.context         = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_list_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final RepoModel reportModel = filterRepoList.get(position);

        holder.tv_name.setText(reportModel.getRepo_name());
        holder.tv_repo_full_name.setText(reportModel.getRepo_full_name());
        holder.tv_views.setText(reportModel.getViews());
        holder.tv_fork.setText(reportModel.getForks());
        holder.tv_stars.setText(reportModel.getStars());

        Picasso.with(context).load(reportModel.getImage_url()).into(holder.iv_avtar);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent repoDetais = new Intent(context,RepoDetailsActivity.class);
                repoDetais.putExtra("repoData",reportModel);
                context.startActivity(repoDetais);

            }
        });
    }

    @Override
    public Filter getFilter() {
        if(repoFilter == null)
            repoFilter = new RepoFilter(this, originalList);
        return repoFilter;
    }
    private static class RepoFilter extends Filter {

        private final RepoListAdapter adapter;

        private final List<RepoModel> originalList;

        private final List<RepoModel> filteredList;

        private RepoFilter(RepoListAdapter adapter, List<RepoModel> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (final RepoModel user : originalList) {

                    if (user.getRepo_name().toLowerCase().trim().contains(filterPattern)) {
                        filteredList.add(user);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filterRepoList.clear();
            adapter.filterRepoList.addAll((ArrayList<RepoModel>) results.values);
            adapter.notifyDataSetChanged();
        }
    }


    @Override
    public int getItemCount() {
        return originalList.size();
    }

}

