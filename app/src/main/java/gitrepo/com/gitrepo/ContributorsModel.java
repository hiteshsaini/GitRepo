package gitrepo.com.gitrepo;

import android.os.Parcel;
import android.os.Parcelable;

public class ContributorsModel implements Parcelable {

    String sno;
    String id;
    String contri_name;
    String repo_url;

    String image_url;



    public ContributorsModel(){

    }

    protected ContributorsModel(Parcel in) {
        sno = in.readString();
        id = in.readString();
        contri_name = in.readString();
        image_url = in.readString();
        repo_url = in.readString();

    }

    public static final Creator<ContributorsModel> CREATOR = new Creator<ContributorsModel>() {
        @Override
        public ContributorsModel createFromParcel(Parcel in) {
            return new ContributorsModel(in);
        }

        @Override
        public ContributorsModel[] newArray(int size) {
            return new ContributorsModel[size];
        }
    };

   /* public ReportModel(int imageId, String title_pranayama, String subtitle_pranayama, int position) {
        this.imageId = imageId;
        this.title_pranayama = title_pranayama;
        this.subtitle_pranayama = subtitle_pranayama;
        this.position = position;
    }*/

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getSno() {
        return sno;
    }
    public void setSno(String sno) {
        this.sno = sno;
    }


    public String getContri_name() {
        return contri_name;
    }
    public void setContri_name(String contri_name) {
        this.contri_name = contri_name;
    }


    public String getImage_url() {
        return image_url;
    }
    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getRepo_url() {
        return repo_url;
    }
    public void setRepo_url(String repo_url) {
        this.repo_url = repo_url;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sno);
        dest.writeString(id);
        dest.writeString(contri_name);

        dest.writeString(image_url);
        dest.writeString(repo_url);

    }

}
