package gitrepo.com.gitrepo;

import android.os.Parcel;
import android.os.Parcelable;

public class RepoModel implements Parcelable {

    String sno;
    String id;
    String repo_name;
    String repo_full_name;
    String repo_description;
    String project_url;
    String contributor_url;

    String views,forks,stars;

    String image_url,createDate;
    String tag_id;


    public RepoModel(){

    }

    protected RepoModel(Parcel in) {
        sno = in.readString();
        id = in.readString();
        repo_name = in.readString();
        repo_full_name = in.readString();
        repo_description = in.readString();
        project_url = in.readString();
        views = in.readString();
        forks = in.readString();
        stars = in.readString();

        image_url = in.readString();
        contributor_url = in.readString();
        createDate = in.readString();
        tag_id = in.readString();

    }

    public static final Creator<RepoModel> CREATOR = new Creator<RepoModel>() {
        @Override
        public RepoModel createFromParcel(Parcel in) {
            return new RepoModel(in);
        }

        @Override
        public RepoModel[] newArray(int size) {
            return new RepoModel[size];
        }
    };

   /* public ReportModel(int imageId, String title_pranayama, String subtitle_pranayama, int position) {
        this.imageId = imageId;
        this.title_pranayama = title_pranayama;
        this.subtitle_pranayama = subtitle_pranayama;
        this.position = position;
    }*/

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getSno() {
        return sno;
    }
    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getRepo_full_name() {
        return repo_full_name;
    }
    public void setRepo_full_name(String repo_full_name) {
        this.repo_full_name = repo_full_name;
    }

    public String getRepo_description() {
        return repo_description;
    }
    public void setRepo_description(String repo_description) {
        this.repo_description = repo_description;
    }

    public String getRepo_name() {
        return repo_name;
    }
    public void setRepo_name(String repo_name) {
        this.repo_name = repo_name;
    }

    public String getViews() {
        return views;
    }
    public void setViews(String views) {
        this.views = views;
    }

    public String getForks() {
        return forks;
    }
    public void setForks(String forks) {
        this.forks = forks;
    }

    public String getStars() {
        return stars;
    }
    public void setStars(String stars) {
        this.stars = stars;
    }

    public String getProject_url() {
        return project_url;
    }
    public void setProject_url(String project_url) {
        this.project_url = project_url;
    }



    public String getImage_url() {
        return image_url;
    }
    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getContributor_url() {
        return contributor_url;
    }
    public void setContributor_url(String contributor_url) {
        this.contributor_url = contributor_url;
    }

    public String getTag_id() {
        return tag_id;
    }
    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getCreateDate() {
        return createDate;
    }
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sno);
        dest.writeString(id);
        dest.writeString(repo_name);
        dest.writeString(repo_full_name);
        dest.writeString(repo_description);
        dest.writeString(project_url);
        dest.writeString(views);
        dest.writeString(forks);
        dest.writeString(stars);

        dest.writeString(image_url);
        dest.writeString(contributor_url);
        dest.writeString(createDate);
        dest.writeString(tag_id);
    }

}
