package gitrepo.com.gitrepo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewAnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {

    Context context;
    FloatingActionButton fab;
    RecyclerView rv_repos;
    Toolbar toolbar;
    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;
    RepoListAdapter adapter;
    List<RepoModel> repoLIst = new ArrayList<>();
    String sort_by="watchers";
    String order_by="desc";
    TextView tv_from;
    TextView tv_to;
    int sendFrom=1;
    String date_from="",date_to="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context         = this;
        toolbar         = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab             = (FloatingActionButton) findViewById(R.id.fab);
        rv_repos        = (RecyclerView) findViewById(R.id.rv_repos);

        getSupportActionBar().setTitle("Home");

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                customAlertDialog(MainActivity.this);
            }
        });

        new DoPostOperation().execute();
    }

    public class DoPostOperation extends AsyncTask<String, Void, String> {
        String result="";
        ProgressDialog progressDialog = new ProgressDialog(context);

        public DoPostOperation() {
            super();
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();

                String postURL = "https://api.github.com/search/repositories?page=1&per_page=10&q=sandeep+created:"+date_from+".."+date_to+"&sort="+sort_by+"&order="+order_by;
                if (date_from.equals("") || date_to.equals("")){
                    postURL = "https://api.github.com/search/repositories?page=1&per_page=10&q=sandeep&sort="+sort_by+"&order="+order_by;
                }
                String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
                String urlEncoded = Uri.encode(postURL, ALLOWED_URI_CHARS);

                HttpGet get = new HttpGet(urlEncoded);

                HttpResponse responsePOST = client.execute(get);
              //  HttpEntity resEntity = responsePOST.getEntity();

                result = EntityUtils.toString(responsePOST.getEntity());
                Log.i("result",result);

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result != null){
                try {
                    JSONObject repositoryData = new JSONObject(result);
                    JSONArray repoArr = repositoryData.getJSONArray("items");
                    List<RepoModel> repoLIst = new ArrayList<>();
                    for (int i = 0; i<repoArr.length();i++){
                        JSONObject data = repoArr.getJSONObject(i);
                       // Log.e("datas",data.toString());
                        System.out.println(data.toString());
                        RepoModel repoModel = new RepoModel();
                        repoModel.setRepo_name(data.getString("name"));
                        repoModel.setRepo_full_name(data.getString("full_name"));
                        repoModel.setRepo_description(data.getString("description"));
                        repoModel.setViews(data.getString("watchers_count"));
                        repoModel.setForks(data.getString("forks_count"));
                        repoModel.setStars(data.getString("stargazers_count"));
                        repoModel.setProject_url(data.getString("html_url"));
                        repoModel.setContributor_url(data.getString("contributors_url"));
                        JSONObject owner = data.getJSONObject("owner");
                        repoModel.setImage_url(owner.getString("avatar_url"));
                        repoLIst.add(repoModel);
                    }

                    adapter = new RepoListAdapter(context,repoLIst);
                    rv_repos.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public final void customAlertDialog(final Activity mActivity)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        final View view = LayoutInflater.from(mActivity).inflate(R.layout.filter_dialog,null);
        alertDialogBuilder.setView(view);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final Button bt_stars   = (Button) view.findViewById(R.id.bt_stars);
        final Button bt_forks   = (Button) view.findViewById(R.id.bt_forks);
        final Button bt_updated = (Button) view.findViewById(R.id.bt_updated);
        final Button bt_desc    = (Button) view.findViewById(R.id.bt_desc);
        final Button bt_asc     = (Button) view.findViewById(R.id.bt_asc);
        Button bt_cancel        = (Button) view.findViewById(R.id.bt_cancel);
        Button bt_reset         = (Button) view.findViewById(R.id.bt_reset);
        final Button bt_sort    = (Button) view.findViewById(R.id.bt_ok);
        LinearLayout bt_from    = (LinearLayout) view.findViewById(R.id.ll_from);
        LinearLayout bt_to      = (LinearLayout) view.findViewById(R.id.ll_to);
        tv_from                 = (TextView) view.findViewById(R.id.tv_from);
        tv_to                   = (TextView) view.findViewById(R.id.tv_to);


        TextView tvOk     = (TextView) view.findViewById(R.id.tv_ok);
        final Animator[] animHide = {null};
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            alertDialog.show();
            view.post(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    int cx = (int)view.getWidth() / 2;
                    int cy = (int) view.getHeight() / 2;
                    float finalRadius = (float) Math.hypot(cx, cy);
                    Animator animVisible = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);
                    animHide[0] = ViewAnimationUtils.createCircularReveal(view, cx, cy, finalRadius, 0);
                    animVisible.start();
                }
            });
        }

        tv_from.setText(date_from);
        tv_to.setText(date_to);
        if (sort_by.equals("stars")){
            bt_forks.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
            bt_stars.setBackground(getResources().getDrawable(R.drawable.stroke));
            bt_updated.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
        } else if (sort_by.equals("forks")){
            bt_forks.setBackground(getResources().getDrawable(R.drawable.stroke));
            bt_stars.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
            bt_updated.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
        }else if (sort_by.equals("updated")){
            bt_forks.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
            bt_stars.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
            bt_updated.setBackground(getResources().getDrawable(R.drawable.stroke));
        }

        if (order_by.equals("asc")){
            bt_asc.setBackground(getResources().getDrawable(R.drawable.stroke));
            bt_desc.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
        } else {
            bt_asc.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
            bt_desc.setBackground(getResources().getDrawable(R.drawable.stroke));
        }

        bt_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sort_by="watchers";
                order_by="desc";
                sendFrom=1;
                date_from="";
                date_to="";
                new DoPostOperation().execute();
                alertDialog.dismiss();
            }
        });

        bt_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sendFrom=1;
                DateDialog();
            }
        });

        bt_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sendFrom=2;
                DateDialog();
            }
        });

        bt_stars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sort_by="stars";
                bt_forks.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
                bt_stars.setBackground(getResources().getDrawable(R.drawable.stroke));
                bt_updated.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
            }
        });

        bt_forks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sort_by="forks";
                bt_forks.setBackground(getResources().getDrawable(R.drawable.stroke));
                bt_stars.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
                bt_updated.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
            }
        });

        bt_updated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sort_by="updated";
                bt_forks.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
                bt_stars.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
                bt_updated.setBackground(getResources().getDrawable(R.drawable.stroke));
            }
        });

        bt_asc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                order_by="asc";
                bt_asc.setBackground(getResources().getDrawable(R.drawable.stroke));
                bt_desc.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));

            }
        });
        bt_desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                order_by="desc";
                bt_asc.setBackground(getResources().getDrawable(R.drawable.stroke_disselect));
                bt_desc.setBackground(getResources().getDrawable(R.drawable.stroke));

            }
        });
        bt_sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DoPostOperation().execute();
                alertDialog.dismiss();
            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    if ( animHide[0]!=null)
                    {
                        animHide[0].addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                alertDialog.dismiss();
                            }
                        });
                        animHide[0].start();
                    }
                    else
                    {
                        alertDialog.dismiss();
                    }
                }
                else
                {
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.setCancelable(false);
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu) {
        getMenuInflater().inflate( R.menu.menu, menu);

        final MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print

                if( ! searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                if (adapter!=null) adapter.getFilter().filter(s);
                //Log.e("changed",s);
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });
        return true;
    }

    public void DateDialog(){

        DatePickerDialog.OnDateSetListener listener=new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth)
            {
                int month = monthOfYear+1;
                DecimalFormat formatter = new DecimalFormat("00");
                if (sendFrom==1){
                    date_from=year+"-"+formatter.format(month)+"-"+formatter.format(dayOfMonth);
                    tv_from.setText(formatter.format(dayOfMonth)+"-"+formatter.format(month)+"-"+year);
                } else {
                    date_to=year+"-"+formatter.format(month)+"-"+formatter.format(dayOfMonth);
                    tv_to.setText(formatter.format(dayOfMonth)+"-"+formatter.format(month)+"-"+year);
                }


            }};

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        DatePickerDialog dpDialog=new DatePickerDialog(this, listener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
        dpDialog.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                // Not implemented here
                return false;
            default:
                break;
        }
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onOptionsItemSelected(item);
    }
}
