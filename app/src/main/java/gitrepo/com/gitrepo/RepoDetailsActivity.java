package gitrepo.com.gitrepo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RepoDetailsActivity extends AppCompatActivity {
    Context context;
    TextView tv_pro_link,tv_pro_dis;
    RecyclerView rv_contri;
    RepoModel repoModel;
    private GridLayoutManager gridLayoutManager;
    ArrayList<ContributorsModel> contriList= new ArrayList<>();
    ContributorsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_details);

        context         = this;
        tv_pro_link     = (TextView) findViewById(R.id.tv_projectlink);
        tv_pro_dis      = (TextView) findViewById(R.id.tv_projectdescription);
        rv_contri       = (RecyclerView) findViewById(R.id.rv_contri);

        gridLayoutManager = new GridLayoutManager(context,3);
        rv_contri.setLayoutManager(gridLayoutManager);

        if (getIntent().getExtras()!=null){
            repoModel = getIntent().getParcelableExtra("repoData");

            tv_pro_link.setText(repoModel.getProject_url());
            tv_pro_dis.setText(repoModel.getRepo_description());

            tv_pro_link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent projectDetails = new Intent(context,ProjectDetailsActivity.class);
                    projectDetails.putExtra("url",repoModel.getProject_url());
                    startActivity(projectDetails);
                }
            });

            new ContributorsList().execute();

            getSupportActionBar().setTitle(repoModel.getRepo_name());
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public class ContributorsList extends AsyncTask<String, Void, String> {
        String result="";
        ProgressDialog progressDialog = new ProgressDialog(context);

        public ContributorsList() {
            super();
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                String postURL = repoModel.getContributor_url();
                HttpGet get = new HttpGet(postURL);

                //  List<NameValuePair> parameters = new ArrayList<NameValuePair>();
                //  parameters.add(new BasicNameValuePair("concept", "" + mOperationId));
                //  parameters.add(new BasicNameValuePair("amount", "" + mAmount));
                //UrlEncodedFormEntity ent = new UrlEncodedFormEntity(parameters, HTTP.UTF_8);
                //  get.setEntity(ent);

                HttpResponse responsePOST = client.execute(get);
                //  HttpEntity resEntity = responsePOST.getEntity();

                result = EntityUtils.toString(responsePOST.getEntity());
                Log.i("result contributors",result);

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result != null){
                try {
                    JSONArray contriAr = new JSONArray(result);
                    for (int i = 0; i<contriAr.length();i++){
                        JSONObject data = contriAr.getJSONObject(i);
                        ContributorsModel contributorsModel = new ContributorsModel();
                        contributorsModel.setContri_name(data.getString("login"));
                        contributorsModel.setId(data.getString("id"));
                        contributorsModel.setImage_url(data.getString("avatar_url"));
                        contributorsModel.setRepo_url(data.getString("repos_url"));

                        contriList.add(contributorsModel);
                    }

                    adapter = new ContributorsAdapter(context,contriList);
                    rv_contri.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
