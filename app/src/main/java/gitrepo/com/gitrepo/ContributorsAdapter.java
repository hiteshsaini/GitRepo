package gitrepo.com.gitrepo;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ContributorsAdapter extends RecyclerView.Adapter<ContributorsAdapter.RecyclerViewHolders> {
    private ArrayList<ContributorsModel> itemList;
    private Context context;


    public ContributorsAdapter(Context context, ArrayList<ContributorsModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contributors_item, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {

        holder.title.setText(Html.fromHtml(itemList.get(position).getContri_name()));

       // holder.icon.setImageResource(itemList.get(position).getCategory_image());
        Picasso.with(context).load(itemList.get(position).getImage_url()).into(holder.icon);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contriDetais = new Intent(context,ContributorDetailActivity.class);
                contriDetais.putExtra("contriData",itemList.get(position));
                context.startActivity(contriDetais);

            }
        });

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder {

        TextView title;
        ImageView icon;


        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        public RecyclerViewHolders(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_category);
            icon = (ImageView) itemView.findViewById(R.id.icon);

        }

    }


}
