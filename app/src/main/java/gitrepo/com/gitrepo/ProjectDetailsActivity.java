package gitrepo.com.gitrepo;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ProjectDetailsActivity extends AppCompatActivity {
    Context context;
    WebView wv_details;
    String detailsUrl="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_details);

        context         = this;
        wv_details      = (WebView) findViewById(R.id.wv_details);

        if (getIntent().getExtras()!=null){
            detailsUrl = getIntent().getStringExtra("url");

            wv_details.getSettings().setJavaScriptEnabled(true);
            wv_details.setWebViewClient(new WebViewClient());
            wv_details.loadUrl(detailsUrl);
        }

        getSupportActionBar().setTitle("Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (wv_details.canGoBack()) {
                    wv_details.goBack();
                } else {
                    onBackPressed();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
